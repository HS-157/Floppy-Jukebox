from mpd import MPDClient
from time import sleep

# Function for play a playlist
def play_list(playlist):
    # We creat mpd client
    client = MPDClient()
    # Set timeout
    client.timeout = 10
    # Set idle timeout
    client.idletimeout = None
    # Set a adresse for mpd server
    client.connect("localhost", 52913)
    # We clear a current playlist
    client.clear()
    # We add a playlist in current playlist
    client.load(playlist)
    # Sleep
    sleep(0.3)
    # We play a current playlist
    client.play()
    # We close and disconnect a mpd client
    client.close()
    client.disconnect()   
