from pyudev import Context, Monitor, MonitorObserver
from mpd_client import play_list
import json

# Generate context
# https://pyudev.readthedocs.io/en/latest/guide.html#id4
context = Context()
monitor = Monitor.from_netlink(context)

# Function for read a json file
def read_json(json_file):
    # Open json file
    with open(json_file, 'r') as f:
        # Load data from json file
        data = json.load(f)
    # Return a data
    return data

# Define a global var
global env
# Creat a empty dict
env = {}
# Var for toggle plug usb key
env["up"] = False

# Function who call when a usb key is plug / unplug
def print_device_event(d):
    # If usb key is plug
    if d.action == "add" and d.get('ID_SERIAL_SHORT') and env.get("up") == False:
        # We get a id
        env["USEC_INITIALIZED"] = d.get("USEC_INITIALIZED")
        # We set a usb key is plug
        env["up"] = True
        # We get data from json file
        id_usb = read_json("id.json")
        # Print a ID
        print(d.get('ID_SERIAL_SHORT'))
        print("Add device")
        # We read if usb key ID is in json data
        for i,j in id_usb.items():
            # If usb key is in data
            if d.get('ID_SERIAL_SHORT') == i:
                # We lunch a playlist
                play_list(j)
                # And exit a loop
                break

    # If usb key is unplug
    elif d.action == "remove" and env["up"] and d.get("USEC_INITIALIZED") == env.get("USEC_INITIALIZED"):
        # We print
        print("Remove device")
        # We set a usb key is unplug
        env["up"] = False

# We generate a function for watch when usb key is plug
observer = MonitorObserver(monitor, callback=print_device_event, name='monitor-observer')
# Start and lunch as daemon
observer.daemon
observer.start()
