# Floppy-Jukebox

A jukebox with floppy (futur)

A jukebox with usb keys (now)

## Need

- Python3
- Pyudev
- Python-mpd2
- MPD

## How use ?

Modify a `id.json` with ID usb and name of playlist.

And lunch `main.py` :

~~~
python3 -i main.py
~~~

And plug usb key for lunch playlist and music !
